Best served with [pi-hole](https://github.com/pi-hole/pi-hole) and [Blokada](https://f-droid.org/en/packages/org.blokada.alarm/).

Work in progress. Permalinks and list composition will change.

```
https://gitlab.com/nobodysu/spyware-hosts/-/raw/master/lists/any-basic.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/android-basic.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/android-mild.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/android-crippling.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/facebook-all.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/games-basic.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/nvidia-mild.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/google-basic.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/mobile-autobill-RU.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/firefox-mild.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/qualcomm-mild.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/swiftkey-all.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/unity-all.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/privacy-instead-of-security.txt
https://gitlab.com/nobodysu/spyware-hosts/raw/master/lists/unsorted.txt
```

`facebook-all.txt` was taken from https://github.com/jmdugan/blocklists.

pi-hole blacklist reciples:

https://gitlab.com/nobodysu/spyware-hosts/blob/master/regex/regex.list



Related links:

https://discourse.pi-hole.net/t/commonly-whitelisted-domains/212

https://www.reddit.com/r/degoogle/comments/cldohl/how_to_degoogle_lineageos_in_2019/